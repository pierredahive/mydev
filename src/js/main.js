/**
 * main.js : Entry point in my wonderfull frontend app
 */

import { openModal, closeModal } from './modal.js';
import { sendForm } from './form.js';

document.addEventListener('DOMContentLoaded', () => {
    const title = [...document.getElementsByTagName('h1')][0];
    title.innerText = 'GIT';
    closeModal();
});
openModal();

sendForm();