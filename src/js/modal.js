/**
* modal.js : Popup and close a modal
*/

export const openModal = () => {
    console.log('Hi, i open a modal');
    const outerDiv = makeDiv('outer-modal');
    outerDiv.appendChild(makeDiv('inner-modal', 'Hello, je suis la modale'));

    document.querySelector('body').appendChild(outerDiv);
}

export const closeModal = () => {
    const modal = document.querySelector('.outer-modal');
    setTimeout(
        () => modal.remove(),
        2000
    );
}

const makeDiv = (className, ...args) => {
    const div = document.createElement('div');
    div.classList.add(className);
    if (args.length) {
        div.innerHTML = args[0];
    }
    return div;
}
